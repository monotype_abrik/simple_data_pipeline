# Home Assignment

The task is to build a simple data pipeline that will upload news (NewsApi) to S3 bucket.

Current implementation includes Apache Airflow workflow management platform installation 
under Docker. 

# News API
News API provides A simple REST API that can be used to retrieve breaking headlines and
search for articles.

You can find more information here: https://newsapi.org/docs

# Run development Airflow instance under Docker
To run development Airflow instance under Docker use the Makefile, that is located under `<project root>/docker/` folder.

## Prerequisites:
* Docker Compose version >= 1.29.0 (Just upgrade your Docker Desktop to the latest version [MacOS, Windows])
* A free News API account is required to obtain an API key
* AWS account and S3 bucket
* Find `<project root>/files/secret_backend_connections.yaml` configuration file 
and replace placeholders with the actual secrets

**Note**: For local development config is used to store NewsApi and AWS credentials. 
For production purposes external secrets backend must be used. 
FYI: https://airflow.apache.org/docs/apache-airflow/2.1.2/security/secrets/secrets-backend/index.html

### WARNING
Do not commit your credentials!!!


## Common make targets (commands)
All the commands below should be launched from the `<project root>/docker/`.

### Run containers
```shell
make start
```
### Stop containers
```shell
make stop
```
### Cleanup environment (remove containers, database files and logs)
```shell
make clean
```
### Show help
```shell
make help
```