from airflow import DAG
from airflow.decorators import task
from airflow.models import Variable
from airflow.utils.dates import days_ago

from custom_utils import load_confs
from news_api.hooks.news_api_hook import NewsApiHook
from news_api.transfers.top_headlines_to_s3 import NewsApiTopHeadlinesToS3Operator

confs = load_confs()
dag_config = confs.get('dag_config')
s3_config = confs.get('s3')
news_api_config = confs.get('news_api')


dag_config.update(start_date=days_ago(1))

with DAG(**dag_config) as dag:

    @task(task_id='Get_source_ids_from_News_Api', multiple_outputs=True)
    def get_sources_ids():
        airflow_vars = Variable.get(key=dag_config.get('dag_id'), default_var=dict(), deserialize_json=True)
        hook = NewsApiHook(http_conn_id=news_api_config['conn_id'])
        sources_response = hook.get_sources(**airflow_vars.get('get_sources_arguments', {}))
        sources = sources_response.json().get('sources', [])
        source_ids = [src['id'] for src in sources]
        limit_sources_count = airflow_vars.get('limit_sources_count')
        if limit_sources_count:
            source_ids = source_ids[:limit_sources_count]
        return {'source_id_list': source_ids}

    source_ids_task = get_sources_ids()

    save_top_headlines_to_s3 = NewsApiTopHeadlinesToS3Operator(
        task_id='Save_top_headlines_to_s3',
        news_api_sources=source_ids_task['source_id_list'],
        s3_bucket=s3_config['bucket'],
        s3_conn_id=s3_config['conn_id'],
        news_api_conn_id=news_api_config['conn_id']
    )

    source_ids_task >> save_top_headlines_to_s3
