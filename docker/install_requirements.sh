#!/usr/bin/env bash

AIRFLOW_DOCKER_CONTAINER_LABEL='com.ccc.container_tag=airflow'

# Prerequisites: Should be installed before installing requirements
for container in $(docker ps -f "label=${AIRFLOW_DOCKER_CONTAINER_LABEL}" -f "name=${PROJECT_NAME}" --format "{{.ID}}"); do
  echo "Upgrading pip for ${container}..."
  docker exec -i "${container}" pip --no-cache-dir --disable-pip-version-check install --upgrade pip &
done
wait

for container in $(docker ps -f "label=${AIRFLOW_DOCKER_CONTAINER_LABEL}" -f "name=${PROJECT_NAME}" --format "{{.ID}}"); do
  echo "Installing python requirements for ${container}..."
  docker exec -i "${container}" pip --no-cache-dir install --upgrade -r ./requirements.txt &
done

wait
echo "Completed"