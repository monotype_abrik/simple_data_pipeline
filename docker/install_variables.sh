#!/usr/bin/env bash


for container in $(docker ps --format '{{.ID}}' -f "name=${PROJECT_NAME}.*webserver"); do
  echo "Installing variables with airflow variables import './variables/dev.json'"
  docker exec -i "${container}" airflow variables import "./variables/dev.json" &
done

wait
echo "Completed"