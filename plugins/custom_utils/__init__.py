import inspect
import logging
import os

from custom_utils.yaml_config_reader import YamlConfigReader

logger = logging.getLogger(__name__)


def __get_conf_file_path(base_path):
    return os.path.join(base_path, 'conf.yml')


def __get_caller_module_base_path(frame):
    caller_module = inspect.getmodule(frame[0])
    caller_dirname = os.path.dirname(caller_module.__file__)
    return caller_dirname


def load_confs():
    """
    Load conf.yml file from the caller's folder and convert it to a dict
    :return: dict containing dag-dependent conf(s)
    """

    caller_module_base_path = __get_caller_module_base_path(inspect.stack()[1])
    environment_caller_module_config_path = __get_conf_file_path(caller_module_base_path)

    confs = YamlConfigReader(
            environment_caller_module_config_path,
            overwrite_by_none=True,
            fail_on_missing_files=True,
            fail_on_parsing_errors=True,
            encoding='utf-8'
        ).data
    logger.info("Loaded configurations from yaml: \n {}".format(confs))
    return confs
