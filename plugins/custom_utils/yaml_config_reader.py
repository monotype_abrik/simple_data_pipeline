import copy
import logging
import os
from typing import Any
import yaml


class YamlConfigReader:
    """
    Main class to read Yaml configuration files.
    Can read multiple files and merge them to a single python structure.

    Usage example:
        >>> from pprint import pprint
        >>> reader = YamlConfigReader('../../../azure-pipelines.yml', '../../../pip-install.yml')
        >>> pprint(reader.data)

    :param args: YAML file(s) absolute paths
    :param overwrite_by_none: (default: True)
          Structure `left_struct` of any python data type
          Structure `right_struct` is None
          During merge:
                `left_struct` to `right_struct`, if `overwrite_by_none` is True - `right_struct` overwrites `left_struct`;
                otherwise - leave `left_struct` without changes
    :param fail_on_missing_files: (default: True)
          When set True: raise FileNotFoundError if file doesn't exist
    :param fail_on_parsing_errors: (default: True)
          When set True: raise exception inherited from yaml.YAMLError if yaml file contains errors
    :param encoding: (default: utf-8) encoding, used to read files
    """

    def __init__(self,
                 *args,
                 overwrite_by_none: bool = True,
                 fail_on_missing_files: bool = True,
                 fail_on_parsing_errors: bool = True,
                 encoding: str = 'utf-8'
                 ) -> None:
        self.overwrite_by_none = overwrite_by_none
        self.fail_on_missing_files = fail_on_missing_files
        self.fail_on_parsing_errors = fail_on_parsing_errors
        self.encoding = encoding

        self._data = None
        self._files = []
        self.logger = logging.getLogger(__name__)

        for arg in args:
            file_path = self._normalize_file_path(arg)
            if file_path not in self._files:
                self._files.append(file_path)
                self._read_file(file_path)

    def _normalize_file_path(self, file_path: str) -> str:
        """
        Normalizes `file_path` - makes it absolute and normalized.
        :param file_path: file path needs to be normalized.
        :return: absolute and normalized file path.
        """
        if not os.path.isabs(file_path):
            file_path = os.path.join(os.getcwd(), file_path)
            self.logger.debug('Path extended for yaml config {}'.format(file_path))
        return os.path.normpath(file_path)

    def _read_file(self, file_path: str) -> None:
        """
        Reads file with specified `file_path`, parses it and adds a parsed content to `self._data`.
        :param file_path: file path needs to be read and parsed
        """
        try:
            with open(file_path, 'r', encoding=self.encoding) as f:
                self._load_yaml_config(f)
        except FileNotFoundError:
            if self.fail_on_missing_files:
                raise

    def _load_yaml_config(self, stream) -> None:
        """
        Parses stream, that contains Yaml markup, using yaml.SafeLoader
        :param stream: File stream or string with Yaml markup
        """
        try:
            yaml_data_generator = yaml.safe_load_all(stream)
            for yaml_data in yaml_data_generator:
                if self._data is None:
                    self._data = yaml_data
                else:
                    self._data = self._deep_merge(self._data, yaml_data)
        except yaml.YAMLError:
            if self.fail_on_parsing_errors:
                raise

    def _deep_merge(self, left_struct: Any, right_struct: Any) -> Any:
        """
        Treats python lists as primitive type.
        Merge behaviour:
            If `left_struct` and `right_struct` are dicts - recursive merge
            Otherwise - rewrite a by b

        Example of recursive merge:
            >>> reader = YamlConfigReader()
            >>> left = {'a': {'a1': 1, 'a2': 2}, 'c': [1, 2]}
            >>> right = {'a': {'a2': 8, 'a3': 3}, 'b': 'd', 'c': [3, 4]}
            >>> reader._deep_merge(left, right)
            {'a': {'a1': 1, 'a2': 8, 'a3': 3}, 'b': 'd', 'c': [3, 4]}

        :param left_struct: left structure (python data type) - lower priority
        :param right_struct: right structure (python data type) - higher priority
        :return: merged structure of resulting python data type
        """
        self.logger.debug('>' * 30)
        self.logger.debug('Start merge of\n{left}\nAND\n{right}'.format(left=left_struct, right=right_struct))
        self.logger.debug('-' * 30)

        if right_struct is None and not self.overwrite_by_none:
            self.logger.debug('Passed right structure as None. Skipping.')

        elif isinstance(left_struct, dict) and isinstance(right_struct, dict):
            self.logger.debug('Merge dicts ...\n{left}\nAND\n{right}'.format(
                              left=left_struct, right=right_struct))
            for key in right_struct:
                if key in left_struct:
                    self.logger.debug('Recursive merge of {left} and {right} for key {key}'.format(
                                      left=left_struct[key], right=right_struct[key], key=key))
                    left_struct[key] = self._deep_merge(left_struct[key], right_struct[key])
                else:
                    self.logger.debug('Append a new key {}'.format(key))
                    left_struct[key] = copy.deepcopy(right_struct[key])
        else:
            self.logger.debug('Replace left structure\n{left}\nwith right structure\n{right}'.format(
                              left=left_struct, right=right_struct))
            left_struct = copy.deepcopy(right_struct)

        self.logger.debug('End of deep_merge: return:\n{}'.format(left_struct))
        self.logger.debug('<' * 30)
        return left_struct

    @property
    def yaml_files(self):
        return self._files

    @property
    def data(self):
        return self._data

    def dump(self, **kwargs) -> str:
        """
        Serializes a `self._data` Python object to Yaml format
        :param kwargs: optional named arguments for `yaml.safe_dump`
        :return: produced string, that contains valid Yaml document
        """
        return yaml.safe_dump(self._data, **kwargs)
