from requests import HTTPError
from airflow.providers.http.hooks.http import HttpHook


class NewsApiHook(HttpHook):

    TOP_HEADLINES_ENDPOINT = "top-headlines"
    EVERYTHING_ENDPOINT = "everything"
    SOURCES_ENDPOINT = "sources"

    # The 2-letter ISO 3166-1 code of the country you want to get headlines for.
    countries = {
        "ae", "ar", "at", "au", "be", "bg", "br", "ca", "ch", "cn",
        "co", "cu", "cz", "de", "eg", "es", "fr", "gb", "gr", "hk",
        "hu", "id", "ie", "il", "in", "is", "it", "jp", "kr", "lt",
        "lv", "ma", "mx", "my", "ng", "nl", "no", "nz", "ph", "pk",
        "pl", "pt", "ro", "rs", "ru", "sa", "se", "sg", "si", "sk",
        "th", "tr", "tw", "ua", "us", "ve", "za", "zh",
    }

    # The 2-letter ISO-639-1 code of the language you want to get articles for.
    languages = {"ar", "en", "cn", "de", "es", "fr", "he", "it",
                 "nl", "no", "pt", "ru", "sv", "se", "ud", "zh"}

    # The category you want to get articles for.
    categories = {"business", "entertainment", "general", "health", "science", "sports", "technology"}

    # The order to sort article results in.
    # If not specified, the default is ``"publishedAt"``.
    sort_method = {"relevancy", "popularity", "publishedAt"}

    def __init__(self, **kwargs):
        super().__init__(method="GET", **kwargs)

    def get_top_headlines(
        self, q=None, sources=None, language="en", country=None, category=None, page_size=None, page=None
    ):
        """Call the `/top-headlines` endpoint.

        Fetch live top and breaking headlines.

        This endpoint provides live top and breaking headlines for a country, specific category in a country,
        single source, or multiple sources. You can also search with keywords.  Articles are sorted by the earliest
        date published first.

        :param q: Keywords or a phrase to search for in the article title and body.  See the official News API
            `documentation <https://newsapi.org/docs/endpoints/everything>`_ for search syntax and examples.
        :type q: str or None

        :param sources: A comma-seperated string of identifiers for the news sources or blogs you want headlines from.
            Use :meth:`NewsApiClient.get_sources` to locate these programmatically, or look at the
            `sources index <https://newsapi.org/sources>`_.  **Note**: you can't mix this param with the
            ``country`` or ``category`` params.
        :type sources: str or None

        :param language: The 2-letter ISO-639-1 code of the language you want to get headlines for.
            See :data:`self.languages` for the set of allowed values.
            The default for this method is ``"en"`` (English).  **Note**: this parameter is not mentioned in the
            `/top-headlines documentation <https://newsapi.org/docs/endpoints/top-headlines>`_ as of Sep. 2019,
            but *is* supported by the API.
        :type language: str or None

        :param country: The 2-letter ISO 3166-1 code of the country you want to get headlines for.
            See :data:`self.countries` for the set of allowed values.
            **Note**: you can't mix this parameter with the ``sources`` param.
        :type country: str or None

        :param category: The category you want to get headlines for.
            See :data:`self.categories` for the set of allowed values.
            **Note**: you can't mix this parameter with the ``sources`` param.
        :type category: str or None

        :param page_size: Use this to page through the results if the total results found is
            greater than the page size.
        :type page_size: int or None

        :param page: The number of results to return per page (request).
            20 is the default, 100 is the maximum.
        :type page: int or None

        :return: JSON response as nested Python dictionary.
        :rtype: dict
        :raises NewsAPIException: If the ``"status"`` value of the response is ``"error"`` rather than ``"ok"``.
        """

        payload = {}

        # Keyword/Phrase
        if q is not None:
            if isinstance(q, str):
                payload["q"] = q
            else:
                raise TypeError("keyword/phrase q param should be of type str")

        # Sources
        if (sources is not None) and ((country is not None) or (category is not None)):
            raise ValueError("cannot mix country/category param with sources param.")

        # Sources
        if sources is not None:
            if isinstance(sources, str):
                payload["sources"] = sources
            else:
                raise TypeError("sources param should be of type str")

        # Language
        if language is not None:
            if isinstance(language, str):
                if language in self.languages:
                    payload["language"] = language
                else:
                    raise ValueError("invalid language")
            else:
                raise TypeError("language param should be of type str")

        # Country
        if country is not None:
            if isinstance(country, str):
                if country in self.countries:
                    payload["country"] = country
                else:
                    raise ValueError("invalid country")
            else:
                raise TypeError("country param should be of type str")

        # Category
        if category is not None:
            if isinstance(category, str):
                if category in self.categories:
                    payload["category"] = category
                else:
                    raise ValueError("invalid category")
            else:
                raise TypeError("category param should be of type str")

        # Page Size
        if page_size is not None:
            if type(page_size) == int:
                if 0 <= page_size <= 100:
                    payload["pageSize"] = page_size
                else:
                    raise ValueError("page_size param should be an int between 1 and 100")
            else:
                raise TypeError("page_size param should be an int")

        # Page
        if page is not None:
            if type(page) == int:
                if page > 0:
                    payload["page"] = page
                else:
                    raise ValueError("page param should be an int greater than 0")
            else:
                raise TypeError("page param should be an int")

        return self.run(endpoint=self.TOP_HEADLINES_ENDPOINT, data=payload)

    def get_sources(self, category=None, language=None, country=None):  # noqa: C901
        """Call the `/sources` endpoint.

        Fetch the subset of news publishers that /top-headlines are available from.

        :param category: Find sources that display news of this category.
            See :data:`self.categories` for the set of allowed values.
        :type category: str or None

        :param language: Find sources that display news in a specific language.
            See :data:`self.languages` for the set of allowed values.
        :type language: str or None

        :param country: Find sources that display news in a specific country.
            See :data:`self.countries` for the set of allowed values.
        :type country: str or None

        :return: JSON response as nested Python dictionary.
        :rtype: dict
        :raises NewsAPIException: If the ``"status"`` value of the response is ``"error"`` rather than ``"ok"``.
        """

        payload = {}

        # Language
        if language is not None:
            if isinstance(language, str):
                if language in self.languages:
                    payload["language"] = language
                else:
                    raise ValueError("invalid language")
            else:
                raise TypeError("language param should be of type str")

        # Country
        if country is not None:
            if isinstance(country, str):
                if country in self.countries:
                    payload["country"] = country
                else:
                    raise ValueError("invalid country")
            else:
                raise TypeError("country param should be of type str")

        # Category
        if category is not None:
            if isinstance(category, str):
                if category in self.categories:
                    payload["category"] = category
                else:
                    raise ValueError("invalid category")
            else:
                raise TypeError("category param should be of type str")

        return self.run(endpoint=self.SOURCES_ENDPOINT, data=payload)

    def top_headlines_paginator(self, q=None, sources=None, language="en", country=None, category=None):
        page = 1
        while True:
            try:
                top_headlines_response = self.get_top_headlines(
                    q=q, sources=sources, language=language, country=country, category=category, page=page)
                articles = top_headlines_response.json().get('articles')
                if not articles:  # It's possible to get empty array if there are no more articles
                    break
                yield articles
                page += 1
            except HTTPError as e:
                if e.response.get('code') == 'maximumResultsReached':
                    break
                else:
                    raise
