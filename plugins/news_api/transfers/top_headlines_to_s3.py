import os
import tempfile
from datetime import datetime
from typing import TYPE_CHECKING, Sequence, Optional, Union

import pandas as pd
from airflow.models import BaseOperator
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from slugify import slugify

from news_api.hooks.news_api_hook import NewsApiHook

if TYPE_CHECKING:
    from airflow.utils.context import Context


class NewsApiTopHeadlinesToS3Operator(BaseOperator):

    template_fields: Sequence[str] = ('news_api_sources', 's3_bucket')

    def __init__(self,
                 *,
                 news_api_sources: list,
                 s3_bucket: Optional[str] = None,
                 s3_conn_id: str = 's3_default',
                 s3_verify: Optional[Union[str, bool]] = None,
                 s3_replace: bool = False,
                 s3_encrypt: bool = False,
                 s3_gzip: bool = False,
                 s3_acl_policy: Optional[str] = None,
                 news_api_conn_id: str = 'news_api_default',
                 news_api_language: Optional[str] = None,
                 news_api_keyword: str = None,
                 **kwargs,
                 ):
        super().__init__(**kwargs)
        self.news_api_conn_id = news_api_conn_id
        self.s3_conn_id = s3_conn_id

        self.news_api_sources = news_api_sources
        self.news_api_keyword = news_api_keyword
        self.news_api_language = news_api_language

        self.s3_bucket = s3_bucket
        self.s3_verify = s3_verify
        self.s3_replace = s3_replace
        self.s3_encrypt = s3_encrypt
        self.s3_gzip = s3_gzip
        self.s3_acl_policy = s3_acl_policy

    def execute(self, context: 'Context'):
        s3_hook = S3Hook(aws_conn_id=self.s3_conn_id, verify=self.s3_verify)
        s3_uris = []
        with tempfile.TemporaryDirectory() as tmp_dir:
            for source_id in self.news_api_sources:
                local_file_path = self.load_to_local(source_id=source_id, base_dir=tmp_dir)
                s3_key = os.path.join(
                    os.path.basename(os.path.dirname(local_file_path)),
                    os.path.basename(local_file_path)
                )

                s3_hook.load_file(
                    filename=local_file_path,
                    key=s3_key,
                    bucket_name=self.s3_bucket,
                    replace=self.s3_replace,
                    encrypt=self.s3_encrypt,
                    gzip=self.s3_gzip,
                    acl_policy=self.s3_acl_policy,
                )
                s3_uri = f"s3://{self.s3_bucket}/{s3_key}"
                s3_uris.append(s3_uri)
                self.log.info(f"News-Api data uploaded to S3 at {s3_uri}.")
        return s3_uris

    def load_to_local(self, source_id, base_dir):
        news_api_hook = NewsApiHook(http_conn_id=self.news_api_conn_id)

        source_id_slug = slugify(source_id, separator='_')
        dirname = os.path.join(base_dir, source_id_slug)
        file_name = '{:%Y%m%d%H%M%S}_headlines.csv'.format(datetime.now())
        file_path = os.path.join(dirname, file_name)

        os.makedirs(dirname, exist_ok=True)

        with open(file_path, mode='a') as local_file:
            for articles in news_api_hook.top_headlines_paginator(sources=source_id):
                pd_df = pd.json_normalize(articles, sep='_')
                pd_df.replace(['\\n', '\\r'], ['\\\\n', '\\\\r'], regex=True, inplace=True)
                pd_df.to_csv(local_file, sep="|", mode="a", index=False, quoting=1,
                             header=(not local_file.tell()))
        self.log.info(f"News-Api data downloaded to local at {file_path}.")
        return file_path

